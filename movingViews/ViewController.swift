//
//  ViewController.swift
//  movingViews
//
//  Created by Hackintosh on 1/4/19.
//  Copyright © 2019 Hackintosh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let v1 = UIView()
    let v2 = UIView()
    
    var topConstraint: NSLayoutConstraint!
    var bottomConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .green
        button.setTitle("Press me", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        view.addSubview(button)
        
        button.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor).isActive = true
        button.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor).isActive = true
        button.widthAnchor.constraint(equalToConstant: 100).isActive = true
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        v1.translatesAutoresizingMaskIntoConstraints = false
        v1.backgroundColor = .red
        view.addSubview(v1)
        v1.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1).isActive = true
        v1.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        bottomConstraint = v1.bottomAnchor.constraint(equalTo: view.topAnchor)
        bottomConstraint.isActive = true
        
        v2.translatesAutoresizingMaskIntoConstraints = false
        v2.backgroundColor = .orange
        view.addSubview(v2)
        v2.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1).isActive = true
        v2.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        topConstraint = v2.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        topConstraint.isActive = true
        
        let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        v2.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc func buttonAction(sender: UIButton!) {
        bottomConstraint.isActive = false
        topConstraint.isActive = false
        bottomConstraint = v1.bottomAnchor.constraint(equalTo: view.topAnchor, constant: v1.bounds.height)
        bottomConstraint.isActive = true
        topConstraint = v2.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -v2.bounds.height)
        topConstraint.isActive = true
    }
    
    func changeConstraints(movingView: UIView, topConstant: CGFloat, bottomConstant: CGFloat) {
        topConstraint.isActive = false
        bottomConstraint.isActive = false
        topConstraint = movingView.topAnchor.constraint(equalTo: view.topAnchor, constant: topConstant)
        topConstraint.isActive = true
        bottomConstraint = v1.bottomAnchor.constraint(equalTo: view.topAnchor, constant: bottomConstant)
        bottomConstraint.isActive = true
    }
    
    var initialY = CGFloat()
    var newTranslationY = CGFloat()
    @objc func handlePan(_ gestureRecognizer : UIPanGestureRecognizer) {
        guard let movingView = gestureRecognizer.view else {return}
        guard let height = movingView.superview?.bounds.height else {return}
        let translation = gestureRecognizer.translation(in: movingView.superview)
        var newY = CGFloat()
        
        if gestureRecognizer.state == .began {
            self.initialY = movingView.frame.minY
            self.newTranslationY = initialY
        }
        if gestureRecognizer.state != .cancelled {
            newY = initialY + translation.y
            if gestureRecognizer.state != .ended {
                newTranslationY = newY - movingView.frame.minY
            }
            if newY >= initialY {
                changeConstraints(movingView: movingView, topConstant: newY, bottomConstant: height - newY)
            }
            else {
                changeConstraints(movingView: movingView, topConstant: initialY, bottomConstant: v1.bounds.height)
            }
        }
        if gestureRecognizer.state == .ended {
            UIView.animate(withDuration: 1.0, animations: {
                if self.newTranslationY >= 0 {
                    self.changeConstraints(movingView: movingView, topConstant: height, bottomConstant: 0)
                    movingView.superview?.layoutIfNeeded()
                }
                else {
                    self.changeConstraints(movingView: movingView, topConstant: self.initialY, bottomConstant: self.v1.bounds.height)
                    movingView.superview?.layoutIfNeeded()
                }
            })
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

